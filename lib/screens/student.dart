import 'package:flutter/material.dart';
import 'package:anak2u_attendance/screens/studentdetail.dart';

class Student extends StatefulWidget {
  @override
  _StudentState createState() => _StudentState();
}

class _StudentState extends State<Student> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.amber[600],
        title: Text('School Name - Student'),
      ),
      body: new ListView(
        children: <Widget>[
          new ListTile(
              leading: new Image.asset('assets/logos/studenticon.png'),
              contentPadding: EdgeInsets.all(20),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => StudentDetail()),
                );
              },
              title: Text('Student Name')),
          new ListTile(
              leading: new Image.asset('assets/logos/studenticon.png'),
              contentPadding: EdgeInsets.all(20),
              title: Text('Student Name')),
          new ListTile(
              leading: new Image.asset('assets/logos/studenticon.png'),
              contentPadding: EdgeInsets.all(20),
              title: Text('Student Name')),
          new ListTile(
              leading: new Image.asset('assets/logos/studenticon.png'),
              contentPadding: EdgeInsets.all(20),
              title: Text('Student Name'))
        ],
      ),
    );
  }
}
