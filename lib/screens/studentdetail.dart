import 'package:anak2u_attendance/screens/studentnote.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';

class StudentDetail extends StatefulWidget {
  @override
  _StudentDetailState createState() => _StudentDetailState();
}

class _StudentDetailState extends State<StudentDetail> {
  TimeOfDay _checkintime = new TimeOfDay.now();

  Future<Null> _selectCheckInTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _checkintime);

    if (picked != null && picked != _checkintime) {
      print('Time selected: ${_checkintime.toString()}');
      setState(() {
        _checkintime = picked;
      });
    }
  }

  TimeOfDay _checkouttime = new TimeOfDay.now();

  Future<Null> _selectCheckOutTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _checkouttime);

    if (picked != null && picked != _checkouttime) {
      print('Time selected: ${_checkouttime.toString()}');
      setState(() {
        _checkouttime = picked;
      });
    }
  }

  File imageFile;

  _openGallery(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.setState(() {
      imageFile = picture;
    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState(() {
      imageFile = picture;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: Text('Gallery'),
                    onTap: () {
                      _openGallery(context);
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: Text('Camera'),
                    onTap: () {
                      _openCamera(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget _decideImageView() {
    if (imageFile == null) {
      return Text("No image is selected",style: TextStyle(fontSize: 18));
    } else {
      return Image.file(imageFile, width: 300, height: 300);
    }
  }

  Widget notesCard(context) {
    return Hero(
      tag: 'Notes',
      transitionOnUserGestures: true,
      child: Card(
        color: Colors.amber[600],
        child: InkWell(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                child: Row(
                  children: <Widget>[
                    Text('Notes',
                        style: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text('Written Notes', style: TextStyle(color: Colors.white)),
                  ],
                ),
              )
            ],
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => EditNotesView()));
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.amber[600],
        title: Text('Student Name'),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _decideImageView(),
              RaisedButton(
                onPressed: () {
                  _showChoiceDialog(context);
                },
                child: Text('Select Image',style: TextStyle(fontSize: 18)),
                padding: EdgeInsets.all(8.0),
              ),
              notesCard(context),
              Container(
                color: Colors.amberAccent.withOpacity(0.5),
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Check In:${_checkintime.toString()}',
                            style: TextStyle(fontSize: 18)),
                        SizedBox(width: 33),
                        FlatButton(
                            child: Text(
                              'Select Time',
                              style: TextStyle(fontSize: 16),
                            ),
                            color: Colors.blue,
                            textColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            onPressed: () {
                              _selectCheckInTime(context);
                            }),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('Check Out:${_checkouttime.toString()}',
                            style: TextStyle(fontSize: 18)),
                        SizedBox(width: 20),
                        FlatButton(
                            child: Text(
                              'Select Time',
                              style: TextStyle(fontSize: 16),
                            ),
                            color: Colors.red,
                            textColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            onPressed: () {
                              _selectCheckOutTime(context);
                            }),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
