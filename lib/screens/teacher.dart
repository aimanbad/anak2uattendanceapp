import 'package:anak2u_attendance/screens/teacherdetail.dart';
import 'package:flutter/material.dart';

class Teacher extends StatefulWidget {
  @override
  _TeacherState createState() => _TeacherState();
}

class _TeacherState extends State<Teacher> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.amber[600],
        title: Text('School Name - Teacher'),
      ),
      body: new ListView(
        children: <Widget>[
          new ListTile(
              leading: new Image.asset('assets/logos/teachericon.png'),
              contentPadding: EdgeInsets.all(20),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TeacherDetail()),
                );
              },
              title: Text('Teacher Name')),
          new ListTile(
              leading: new Image.asset('assets/logos/teachericon.png'),
              contentPadding: EdgeInsets.all(20),
              title: Text('Teacher Name')),
        ],
      ),
    );
  }
}

