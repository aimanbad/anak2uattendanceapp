import 'package:anak2u_attendance/screens/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:anak2u_attendance/screens/student.dart';
import 'package:anak2u_attendance/screens/teacher.dart';

class SelectUser extends StatefulWidget {
  @override
  _SelectUserState createState() => _SelectUserState();
}

createAlertDialog(BuildContext context) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Log Out'),
          content: Text('Are you sure you want to log out?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes'),
              textColor: Colors.white,
              color: Colors.teal,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                );
              },
            ),
            FlatButton(
              child: Text('No'),
              textColor: Colors.white,
              color: Colors.pink[300],
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
          elevation: 24.0,
        );
      });
}

class _SelectUserState extends State<SelectUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.amber[600],
        title: Text('School Name'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            color: Colors.white,
            onPressed: () {
              createAlertDialog(context);
            },
          )
        ],
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/logos/teacher.jpg', scale: 3.8),
              SizedBox(height: 5),
              RaisedButton(
                child: Text('Teacher',style: TextStyle(fontSize: 15)),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                color: Colors.teal,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Teacher()),
                  );
                },
              ),
              SizedBox(height: 10),
              Image.asset('assets/logos/student.jpg', scale: 2),
              SizedBox(height: 5),
              RaisedButton(
                child: Text('Student',style: TextStyle(fontSize: 15)),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
                color: Colors.pink[300],
                textColor: Colors.white,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Student()),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
