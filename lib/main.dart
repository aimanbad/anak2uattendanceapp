import 'package:flutter/material.dart';
import 'package:anak2u_attendance/screens/login_screen.dart';
import 'package:flutter/services.dart';

Future main() async {
  await SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
  runApp(new MyApp());
}

class  MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Anak2U Attendance Application',
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}
